AMD_BSP = "${@d.getVar('BBFILE_PATTERN_amd-bsp',d,1).replace('^', '')}"
FILESEXTRAPATHS_prepend := "${AMD_BSP}recipes-kernel/linux/linux-yocto-4.19.8-r1000:"

SRC_URI_append_congatec-tr4-r-64 += "file://r1000-user-features.scc \
    file://r1000-user-patches.scc \
    file://r1000.cfg \
    file://r1000-user-config.cfg \
    file://r1000-gpu-config.cfg \
    file://r1000-extra-config.cfg \
    file://r1000-standard-only.cfg \
"

KERNEL_FEATURES_append_congatec-tr4-r-64 = " cfg/sound.scc"

COMPATIBLE_MACHINE_congatec-tr4-r-64 = "congatec-tr4-r-64"
