AMD_BSP = "${@d.getVar('BBFILE_PATTERN_amd-bsp',d,1).replace('^', '')}"
FILESEXTRAPATHS_prepend := "${AMD_BSP}recipes-kernel/linux/linux-yocto-4.19.8-v1000:"

SRC_URI_append_congatec-tr4-v-64 += "file://v1000-user-features.scc \
    file://v1000-user-patches.scc \
    file://v1000.cfg \
    file://v1000-user-config.cfg \
    file://v1000-gpu-config.cfg \
    file://v1000-extra-config.cfg \
    file://v1000-standard-only.cfg \
"

KERNEL_FEATURES_append_congatec-tr4-v-64 = " cfg/sound.scc"

COMPATIBLE_MACHINE_congatec-tr4-v-64 = "congatec-tr4-v-64"
