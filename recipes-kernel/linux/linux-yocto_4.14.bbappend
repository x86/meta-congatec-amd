MACH = "${@d.getVar('MACHINE')}"
require ${@oe.utils.conditional('MACH', 'congatec-b7e3-64', 'linux-yocto-common_4.14.inc', "", d)}

KBRANCH_amdx86 ?= "v4.14/standard/base"
SRCREV_machine_amdx86 ?= "78a16a4d8cfd58f91be412797aac248e811d083b"

include linux-yocto-${MACHINE}_4.14.inc
