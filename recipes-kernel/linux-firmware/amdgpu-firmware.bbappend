# This will replace congatec product names with silicon name
MACH = "${@d.getVar('MACHINE')}"
AMD_MACH = "${@oe.utils.conditional('MACH', 'congatec-tr4-v-64', 'v1000', \
oe.utils.conditional('MACH', 'congatec-tr4-r-64', 'r1000', \
oe.utils.conditional('MACH', 'congatec-b7e3-64', 'e3000', 'UNKNOWN_MACHINE', d), d), d)}"
AMD_BSP = "${@d.getVar('BBFILE_PATTERN_amd-bsp',d,1).replace('^', '')}"
FILESEXTRAPATHS_prepend := "${AMD_BSP}recipes-kernel/linux-firmware/amdgpu-firmware-${AMD_MACH}:"
