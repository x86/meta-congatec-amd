DESCRIPTION = "Congatec OS API (CGOS) support"
SECTION = "libs"
AUTHOR = "jan.janson@congatec.com"
SECTION = "console/utils"
HOMEPAGE = "http://www.congatec.com/en/products/qseven/conga-qa3.html#tab3"
BUGTRACKER = "https://git.congatec.com/x86/meta-congatec-x86/issues"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${WORKDIR}/cgoslx-x64-1.03.025/CgosDrv/COPYING_GPL2;md5=b234ee4d69f5fce4486a80fdaf4a4263"

COMPATIBLE_MACHINE = "congatec-(qa3|tca4|tca5|b7ac|tsco|tcwl|tr4-r|tr4-v|b7e3)-64"

PR = "r5"

inherit module

SRC_URI = "file://cgoslx-x64-1.03.025.tar.gz \
           file://cgoslx-x64-1.03.025.patch \
           file://cgos-mod_001.patch \
          "

S = "${WORKDIR}/cgoslx-x64-1.03.025"
EXTRA_OEMAKE = " KERNELDIR=${STAGING_KERNEL_DIR} KERNELINST=${D}"

do_compile() {
  oe_runmake mod
}

do_install() {
  oe_runmake install_mod
}

PARALLEL_MAKE = ""
MACHINE_EXTRA_RRECOMMENDS += "kernel-module-cgosdrv"
