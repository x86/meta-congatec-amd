DESCRIPTION = "Congatec OS API (CGOS) support"
AUTHOR = "jan.janson@congatec.com"
SECTION = "console/utils"
HOMEPAGE = "http://www.congatec.com/en/products/qseven/conga-qa3.html#tab3"
BUGTRACKER = "https://git.congatec.com/x86/meta-congatec-x86/issues"
LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://${WORKDIR}/cgoslx-x64-1.03.025/CgosLib/COPYING_BSD2;md5=ed45fbc9a3094e85642610390226ff9c"

COMPATIBLE_MACHINE = "congatec-(qa3|tca4|tca5|b7ac|tsco|tcwl|tr4-r|tr4-v|b7e3)-64"

PR = "r6"

DEPENDS = " cgos-mod"
RDEPENDS_${PN} = " cgos-mod"

SRC_URI = "file://cgoslx-x64-1.03.025.tar.gz \
           file://cgoslx-x64-1.03.025.patch \
          "

PACKAGES = "${PN} ${PN}-dev ${PN}-dbg"
PROVIDES = "${PACKAGES}"

FILES_${PN} += "${bindir}/cgosdump \
                ${bindir}/cgosmon \
                ${libdir}/libcgos.so \
               "

FILES_${PN}-dev += " ${includedir}/Cgos.h \
                     ${libdir}/libcgos.so \
                   "

FILES_${PN}-dbg += " ${libdir}/.debug/* \
                     ${bindir}/.debug/* \
                   "

S = "${WORKDIR}/cgoslx-x64-1.03.025"
EXTRA_OEMAKE = " INST_BIN=${D}${bindir} INST_LIB=${D}${libdir}"

do_compile() {
  oe_runmake app
}

do_install() {
  oe_runmake install_app

  install -d ${D}/${includedir}
  install -m 0644 ${S}/CgosLib/Cgos.h ${D}/${includedir}
  install -m 0755 ${S}/CgosLib/Lx/libcgos.so ${D}/${libdir}
}

PARALLEL_MAKE = ""
