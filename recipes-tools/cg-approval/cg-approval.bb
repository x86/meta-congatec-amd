DESCRIPTION = "Congatec Approval Test Dependencies"
AUTHOR = "jan.janson@congatec.com"
SECTION = "devel"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${WORKDIR}/COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

PR = "r1"
ALLOW_EMPTY_${PN} = "1"

SRC_URI = "file://COPYING \
"

RRECOMMENDS_${PN} = " \
  networkmanager systemd systemd-compat-units cgutil pciutils \
  ethtool dropbear util-linux nano bash \
  alsa-utils setserial \
  iptables  iputils iproute2 \
  iw crda wpa-supplicant tcpdump iperf3 expect socat \
  modemmanager ppp screen usb-modeswitch usbutils \
  kernel-module-bluetooth bluez-hcidump bluez5-noinst-tools \
  hostapd wpa-supplicant conntrack-tools \
"
#kernel-modules

# As this package is tied to systemd, only build it when we're also building systemd.
python () {
  if not bb.utils.contains ('DISTRO_FEATURES', 'systemd', True, False, d):
    bb.warn("add to local.conf:")
    bb.warn("DISTRO_FEATURES_append = \" systemd\"")
    bb.warn("VIRTUAL-RUNTIME_init_manager = \"systemd\"")
    bb.warn("DISTRO_FEATURES_BACKFILL_CONSIDERED = \"sysvinit\"")
    bb.warn("VIRTUAL-RUNTIME_initscripts = \"\"")
    raise bb.parse.SkipPackage("'systemd' not in DISTRO_FEATURES")
}

