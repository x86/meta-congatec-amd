DESCRIPTION = "Congatec System Utility"
AUTHOR = "jan.janson@congatec.com"
SECTION = "console/utils"
HOMEPAGE = "http://www.congatec.com/en/products/qseven/conga-qa3.html#tab3"
BUGTRACKER = "https://git.congatec.com/x86/meta-congatec-x86/issues"

LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://${WORKDIR}/cgutillx/COPYING_BSD2;md5=ed45fbc9a3094e85642610390226ff9c"

COMPATIBLE_MACHINE = "congatec-(qa3|tca4|tca5|b7ac|tsco|tcwl|tr4-r|tr4-v|b7e3)-64"

PR = "r4"

DEPENDS = "cgos"
RDEPENDS_${PN} = " cgos"

SRC_URI = "file://cgutillx_1.5.6.tar \
           file://cgutillx_1.5.6-build.patch \
           file://cgutillx_1.5.6-build-002.patch \
           file://no-pie.patch \
          "

FILES_${PN} += "${bindir}/cgutlcmd"

S = "${WORKDIR}/cgutillx"

do_compile() {
  env
  cd cgutlcmd
  oe_runmake
}

do_install() {
  install -d ${D}/${bindir}
  install -m 0755 ${WORKDIR}/cgutillx/cgutlcmd/cgutlcmd ${D}/${bindir}
}

PARALLEL_MAKE = ""
