#@TYPE: Machine
#@NAME: congtec-tr4-v-64

#@DESCRIPTION: Machine configuration for congtec-tr4-v-64

# BSP and PATCH versions for MEL releases
BSP_VERSION = "1"
PATCH_VERSION = "0"

PREFERRED_PROVIDER_virtual/kernel ?= "linux-yocto"
PREFERRED_VERSION_linux-yocto ?= "4.19%"
PREFERRED_VERSION_rgp ?= "1.5.1"

require conf/machine/include/tune-v1000.inc

# Add machine specific AMD features and feature pkgs here
VULKAN_PKGS_congatec-tr4-v-64 = "amdvlk vulkan vulkan-tools rgp"
AMD_PLATFORM_SPECIFIC_PKGS_congatec-tr4-v-64 += " \
        ${@bb.utils.contains('INCLUDE_OPENCL', 'yes', 'opencl', '', d)} \
        "

include conf/machine/include/amd-common-configurations.inc
include conf/machine/include/amd-customer-configurations.inc

# GPU
XSERVER_X86_GPU = "xf86-video-amd \
           ${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'mesa-driver-radeonsi', '', d)} \
           "

XSERVER ?= "${XSERVER_X86_BASE} \
            ${XSERVER_X86_EXT} \
            ${XSERVER_X86_FBDEV} \
            ${XSERVER_X86_MODESETTING} \
            ${XSERVER_X86_GPU} \
           "

KERNEL_MODULE_AUTOLOAD += "snd-soc-acp-pcm snd-soc-acp-rt286-mach amdgpu"
MACHINE_EXTRA_RRECOMMENDS += "amdgpu-firmware grub-efi"
MACHINE_EXTRA_RRECOMMENDS_remove = "rtc-test smbus-test grub"

# Setup a getty on all serial ports
SERIAL_CONSOLES ?= "115200;ttyS0 115200;ttyS1"

# Enable the kernel console on ttyS4/USB0 as well
KERNEL_SERIAL_CONSOLE ?= "console=ttyS0,115200n8"

MACHINEOVERRIDES =. "amd:amdx86:amdgpu:v1000:"

MACHINE_FEATURES += "alsa"
MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS += "kernel-module-igb kernel-module-e1000e "
